/**
 * Copyright (c) 2019 chrismo
 *
 * See the NOTICE file(s) distributed with this work for additional information.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.nibeheatpump.internal.api;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Defines the methods of the handler API.
 * 
 * @author chrismo - Initial contribution
 */
public interface HandlerApi extends Runnable {
  /**
   * Initialize the API. Has to be called before commands can be executed.
   * 
   * @param in the input stream to read commands from
   * @param out the output stream to write command responses to
   * @param cb the callback for command responses
   */
  public void setUp(InputStream in, OutputStream out, ApiCommandCallback cb);

  /**
   * Handles the given command response, i.e. writes it to the {@link OutputStream} that was set up.
   * 
   * @param response the string response to handle
   */
  public void handleCommandResponse(String response);

  /**
   * Shuts down the API. No further commands can be issued after this method has been called.
   */
  public void shutDown();

}
