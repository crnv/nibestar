/**
 * Copyright (c) 2019 chrismo
 *
 * See the NOTICE file(s) distributed with this work for additional information.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.nibeheatpump.internal.api;

/**
 * Interface that contains method definitions to handle API commands.
 * 
 * @author chrismo - Initial contribution
 */
public interface ApiCommandCallback {

  /**
   * Handle the given command.
   * 
   * @param cmd the command to execute
   */
  public void handleCommand(ApiCommand cmd);

}
