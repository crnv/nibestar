/**
 * Copyright (c) 2019 chrismo
 *
 * See the NOTICE file(s) distributed with this work for additional information.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.nibeheatpump.internal.api;

/**
 * Holds information of an API command.
 * 
 * @author chrismo - Initial contribution
 */
public class ApiCommand {
  public final Type type;
  public final int address;
  public final int value;

  public ApiCommand(Type type, int address, int value) {
    this.type = type;
    this.address = address;
    this.value = value;
  }

  @Override
  public String toString() {
    return type + " " + address + " " + value;
  }

  public static enum Type {

    INFO("info"), QUERY("query"), QUERYALL("queryall"), READ("read"), REFRESH("refresh"), WRITE(
        "write"), SUBSCRIBE("subscribe"), UNSUBSCRIBE("unsubscribe"), QUIT("quit"), USAGE("usage");

    private final String command;

    private Type(String command) {
      this.command = command;
    }

    public String toString() {
      return command;
    }

    public static Type fromString(String text) {
      for (Type t : Type.values()) {
        if (t.command.equalsIgnoreCase(text)) {
          return t;
        }
      }
      return null;
    }
  };
}
