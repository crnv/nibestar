/**
 * Copyright (c) 2019 chrismo
 *
 * See the NOTICE file(s) distributed with this work for additional information.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.nibeheatpump.internal.api;

/**
 * Factory that creates {@link HandlerApi} instances.
 * 
 * @author chrismo - Initial contribution
 */
public class HandlerApiFactory {

  public static HandlerApi getHandlerApi(String type) {
    if (type.equalsIgnoreCase("console")) {
      return new ConsoleApi();
    } else {
      return null;
    }
  }
}
