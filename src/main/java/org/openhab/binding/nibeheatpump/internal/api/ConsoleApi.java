/**
 * Copyright (c) 2019 chrismo
 *
 * See the NOTICE file(s) distributed with this work for additional information.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.nibeheatpump.internal.api;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple API to send commands to the handler and receive responses from it.
 * 
 * @author chrismo - Initial contribution
 */
public class ConsoleApi implements HandlerApi {
  private final Logger logger = LoggerFactory.getLogger(ConsoleApi.class);
  private ApiCommandCallback callback;
  private InputStream in;
  private BufferedOutputStream out;

  @Override
  public void setUp(InputStream in, OutputStream out, ApiCommandCallback cb) {
    this.in = in;
    this.out = new BufferedOutputStream(out);
    this.callback = cb;
    handleCommandResponse("READY (enter USAGE to see usage information)");
  }

  @Override
  public void shutDown() {
    in = null;
  }

  @Override
  public void handleCommandResponse(String response) {
    try {
      out.write(response.getBytes(Charset.forName("UTF-8")));
      out.write('\n');
      out.flush();
    } catch (IOException e) {
      logger.error("Cannot write command response", e);
    }
  }

  @Override
  public void run() {
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    Scanner scanner = null;
    ApiCommand.Type cmdType = null;
    int address = 0;
    int value = 0;

    while (true) {
      try {
        scanner = new Scanner(reader.readLine());
        cmdType = ApiCommand.Type.fromString(scanner.next().toLowerCase());
        address = 0;
        value = 0;

        switch (cmdType) {
          case USAGE:
            printUsage();
            continue;
          case QUIT:
          case QUERYALL:
            if (scanner.hasNext()) {
              logger.warn("Igoring superfluous parameter(s)");
            }
            break;
          case INFO:
          case READ:
          case SUBSCRIBE:
          case UNSUBSCRIBE:
          case QUERY:
            address = scanner.nextInt();
            if (scanner.hasNext()) {
              logger.warn("Igoring superfluous parameter(s)");
            }
            break;
          case WRITE:
            address = scanner.nextInt();
            value = scanner.nextInt();
            if (scanner.hasNext()) {
              logger.warn("Ignoring superfluous parameter(s)");
            }
            break;
          default:
            System.err.println("Could not read command");
            printCmdSyntaxError("unsupported command");
        }
      } catch (NoSuchElementException | IOException | NullPointerException e) {
        String cause = "syntax error";
        if (e instanceof InputMismatchException) {
          cause = "Invalid argument type";
        } else if (e instanceof NoSuchElementException) {
          cause = "missing argument";
        } else if (e instanceof NullPointerException) {
          cause = "unknown command";
        }
        logger.warn("Could not read command: exception");
        printCmdSyntaxError(cause);
        cmdType = null;
      } finally {
        if (scanner != null) {
          scanner.close();
        }
      }

      if (cmdType != null) {
        callback.handleCommand(new ApiCommand(cmdType, address, value));
      }
    }
  }

  private void printCmdSyntaxError(String cause) {
    if (cause == null || cause.isEmpty()) {
      cause = "unknown";
    }
    handleCommandResponse("ERROR: INVALID INPUT (" + cause.toUpperCase() + ")");
  }

  public void printUsage() {
    StringBuffer sb = new StringBuffer();
    if (out != null) {
      sb.append("usage: this API supports the following commands:\n");
      sb.append("\tINFO id: prints information about the variable with the given id\n");
      sb.append("\tQUERY id: query the variable with the given id. The variable has to be included in the LOG.SET file or been manually read out before.\n");
      sb.append("\tQUERYALL id: query all previously read variables.\n");
      sb.append("\tQUIT: quit the program.\n");
      sb.append("\tREAD id: read the variable with the given id. Caveat: the most important variables should be added to LOG.SET file, to be reported periodically by the heat pump\n");
      sb.append("\tSUBSCRIBE id: periodically read the variable with the given id.\n");
      sb.append("\tUNSUBSCRIBE id: stop reading the variable with the given id periodically.\n");
      sb.append("\tUSAGE: print this usage information.\n");
      sb.append("\tWRITE id value: set the variable with the given id to the defined value. Caveat: use with caution!\n");
      
      handleCommandResponse(sb.toString());
    }
  }
}
