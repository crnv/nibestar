/**
 * Copyright (c) 2019 chrismo
 *
 * See the NOTICE file(s) distributed with this work for additional information.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.nibeheatpump.internal.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Creates the Java code that is used to define all variables in the heat pump model definitions.
 * 
 * @author chrismo - Initial contribution
 */
public class VariableInformationReader {

  public static void main(String[] args) {
    String fileName = "export.csv";
    File exportFile;
    if (args.length > 0) {
      fileName = args[0];
    }
    exportFile = new File(fileName);
    BufferedReader reader = null;
    try {
      if (exportFile.exists() && exportFile.isFile() && exportFile.canRead()) {
        reader = new BufferedReader(new FileReader(exportFile));
        String line;

        while ((line = reader.readLine()) != null) {
          // some strings in the export file contain ';' within the quoted info field
          line = replaceSemiColonsInQuotes(line);
          String[] parts = line.split(";");
          if (parts.length == 10) {
            try {
              String title = parts[0];
              // String info = parts[1].replaceAll("\"", "");
              int id = Integer.parseInt(parts[2]);
              // String unit = parts[3];
              String dataType = parts[4];
              int factor = Integer.parseInt(parts[5]);
              int min = Integer.parseInt(parts[6]);
              int max = Integer.parseInt(parts[7]);
              int def = Integer.parseInt(parts[8]);
              String mode = parts[9];

              System.out.println("put(" + id + ", new VariableInformation(" + factor + ", "
                  + getDataType(dataType) + ", " + getMode(mode) + ", " + title + ", " + min + ", "
                  + max + ", " + def + "));");

            } catch (NumberFormatException e) {
              // nothing to do, will only fail for header
            }

          } else {
            System.out.println("// " + line);
          }
        }
      }
    } catch (IOException e) {
      System.err.println("could not read file");
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (IOException ne) {
      }
    }
  }

  private static String replaceSemiColonsInQuotes(String line) {
    boolean inQuote = false;
    StringBuffer result = new StringBuffer(line.length());

    for (int i = 0; i < line.length(); i++) {
      char ch = line.charAt(i);
      if (ch == '"') {
        inQuote = !inQuote;
      } else if (ch == ';' && inQuote) {
        ch = ',';
      }

      result.append(ch);
    }

    return result.toString();
  }

  private static String getDataType(String unit) {
    switch (unit) {
      case "u8":
        return "NibeDataType.U8";
      case "u16":
        return "NibeDataType.U16";
      case "u32":
        return "NibeDataType.U32";
      case "s8":
        return "NibeDataType.S8";
      case "s16":
        return "NibeDataType.S16";
      case "s32":
        return "NibeDataType.S32";
      default:
        return null;
    }
  }

  private static String getMode(String mode) {
    switch (mode) {
      case "R":
        return "Type.SENSOR";
      case "R/W":
        return "Type.SETTING";
      default:
        return null;
    }
  }
}

/**
 * Title;Info;ID;Unit;Size;Factor;Min;Max;Default;Mode "NIBE Inverter
 * 216-state";"";32260;"";u8;1;0;0;0;R; "BT1 Outdoor Temperature";"Current outdoor
 * temperature";40004;"�C";s16;10;0;0;0;R; "EP23-BT2 Supply temp S4";"Supply temperature for system
 * 4";40005;"�C";s16;10;0;0;0;R; "EP22-BT2 Supply temp S3";"Supply temperature for system
 * 3";40006;"�C";s16;10;0;0;0;R; "EP21-BT2 Supply temp S2";"Supply temperature for system
 * 2";40007;"�C";s16;10;0;0;0;R; "BT2 Supply temp S1";"Supply temperature for system
 * 1";40008;"�C";s16;10;0;0;0;R; "EB100-EP14-BT3 Return temp";"Return
 * temperature";40012;"�C";s16;10;0;0;0;R;
 **/
