/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project Modified work Copyright (c) 2019
 * chrismo
 *
 * See the NOTICE file(s) distributed with this work for additional information.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.nibeheatpump;

import org.openhab.binding.nibeheatpump.internal.NibeHeatPumpCommandResult;
import org.openhab.binding.nibeheatpump.internal.NibeHeatPumpException;
import org.openhab.binding.nibeheatpump.internal.api.ApiCommand;
import org.openhab.binding.nibeheatpump.internal.api.ApiCommandCallback;
import org.openhab.binding.nibeheatpump.internal.api.HandlerApi;
import org.openhab.binding.nibeheatpump.internal.api.HandlerApiFactory;
import org.openhab.binding.nibeheatpump.internal.config.NibeHeatPumpConfiguration;
import org.openhab.binding.nibeheatpump.internal.connection.NibeHeatPumpConnector;
import org.openhab.binding.nibeheatpump.internal.connection.NibeHeatPumpEventListener;
import org.openhab.binding.nibeheatpump.internal.connection.UDPConnector;
import org.openhab.binding.nibeheatpump.internal.message.ModbusDataReadOutMessage;
import org.openhab.binding.nibeheatpump.internal.message.ModbusReadRequestMessage;
import org.openhab.binding.nibeheatpump.internal.message.ModbusReadResponseMessage;
import org.openhab.binding.nibeheatpump.internal.message.ModbusValue;
import org.openhab.binding.nibeheatpump.internal.message.ModbusWriteRequestMessage;
import org.openhab.binding.nibeheatpump.internal.message.ModbusWriteResponseMessage;
import org.openhab.binding.nibeheatpump.internal.message.NibeHeatPumpMessage;
import org.openhab.binding.nibeheatpump.internal.models.PumpModel;
import org.openhab.binding.nibeheatpump.internal.models.VariableInformation;
import org.openhab.binding.nibeheatpump.internal.models.VariableInformation.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author chrismo - Modifications to run without openHAB
 * @author Pauli Anttila - Initial contribution
 *
 */
public class NibeHeatPumpStandAloneHandler
    implements NibeHeatPumpEventListener, ApiCommandCallback {

  private final Logger logger = LoggerFactory.getLogger(NibeHeatPumpStandAloneHandler.class);

  private static final int TIMEOUT = 4500;

  private final HandlerApi api;
  private Thread apiThread;

  private final PumpModel pumpModel;

  private NibeHeatPumpConfiguration configuration;

  private NibeHeatPumpConnector connector;
  private boolean reconnectRequest;
  // private Status status = Status.OFFLINE;

  private NibeHeatPumpCommandResult writeResult;
  private NibeHeatPumpCommandResult readResult;

  private ScheduledExecutorService scheduler =
      Executors.newScheduledThreadPool(1, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable runnable) {
          Thread newThread = new Thread(runnable, "Scheduler");
          // newThread.setDaemon(true); // the process may exit even if this thread is
          // still running
          return newThread;
        }
      });
  private ScheduledFuture<?> connectorTask;
  private ScheduledFuture<?> pollingJob;

  private final List<Integer> itemsToPoll = Collections.synchronizedList(new ArrayList<>());

  private final List<Integer> itemsToEnableWrite = new ArrayList<>();

  private final Map<Integer, CacheObject> stateMap =
      Collections.synchronizedMap(new HashMap<Integer, CacheObject>());

  private long lastUpdateTime = 0;

  protected class CacheObject {

    /** Time when cache object updated in milliseconds */
    final long lastUpdateTime;

    /** Cache value */
    final Double value;

    /**
     * Initialize cache object.
     *
     * @param lastUpdateTime Time in milliseconds.
     *
     * @param value Cache value.
     */
    CacheObject(long lastUpdateTime, Double value) {
      this.lastUpdateTime = lastUpdateTime;
      this.value = value;
    }
  }

  private enum Status {
    ONLINE("online"), OFFLINE("offline");

    private final String status;

    private Status(String status) {
      this.status = status;
    }

    public String toString() {
      return status;
    }
  };

  public NibeHeatPumpStandAloneHandler(PumpModel pumpModel, HandlerApi api,
      NibeHeatPumpConfiguration config) {
    this.pumpModel = pumpModel;
    this.api = api;
    this.configuration = config;
    api.setUp(System.in, System.out, this);

  }

  public static void main(String[] args) {
    final Map<String, String> argsMap = new HashMap<String, String>();
    System.out.println("NibeStar: Unofficial Nibe Modbus Interface");
    for (String arg : args) {
      String[] parts = arg.split("=");
      if (parts.length == 2) {
        argsMap.put(parts[0].toLowerCase(), parts[1]);
      }
    }

    String propertiesPath = argsMap.get("configuration");
    String pumpModel = argsMap.get("pumpModel");
    String api = argsMap.get("api");

    NibeHeatPumpConfiguration config =
        NibeHeatPumpStandAloneHandler.getHeatPumpConfigFromProperties(propertiesPath);

    StandAloneHandlerConfiguration handlerConfig =
        NibeHeatPumpStandAloneHandler.getHandlerConfigFromProperties(propertiesPath);

    // override properties with command line arguments
    if (api != null) {
      handlerConfig.api = api;
    }

    if (pumpModel != null) {
      handlerConfig.pumpModel = pumpModel;
    }

    NibeHeatPumpStandAloneHandler handler =
        new NibeHeatPumpStandAloneHandler(PumpModel.getPumpModel(handlerConfig.pumpModel),
            HandlerApiFactory.getHandlerApi(handlerConfig.api), config);

    handler.initialize();

  }

  private static StandAloneHandlerConfiguration getHandlerConfigFromProperties(
      String propertiesPath) {
    StandAloneHandlerConfiguration configuration = new StandAloneHandlerConfiguration();

    if (propertiesPath == null || propertiesPath.isEmpty()) {
      propertiesPath = "default.properties";
    }

    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    InputStream resourceStream = null;
    try {
      resourceStream = loader.getResourceAsStream(propertiesPath);
      if (resourceStream == null) {
        resourceStream = new FileInputStream(new File(propertiesPath));

      }
      Properties props = new Properties();
      props.load(resourceStream);
      configuration.api = props.getProperty("api");
      configuration.pumpModel = props.getProperty("pumpModel");

    } catch (IOException e) {
      // nothing to do;
    } finally {
      try {
        if (resourceStream != null) {
          resourceStream.close();
        }
      } catch (IOException e) {
        // nothing to do;
      }
    }
    return configuration;
  }

  private static NibeHeatPumpConfiguration getHeatPumpConfigFromProperties(String propertiesPath) {
    NibeHeatPumpConfiguration configuration = new NibeHeatPumpConfiguration();

    if (propertiesPath == null || propertiesPath.isEmpty()) {
      propertiesPath = "default.properties";
    }

    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    InputStream resourceStream = null;
    try {
      resourceStream = loader.getResourceAsStream(propertiesPath);
      if (resourceStream == null) {
        resourceStream = new FileInputStream(new File(propertiesPath));

      }
      Properties props = new Properties();
      props.load(resourceStream);

      configuration.hostName = props.getProperty("hostName");
      configuration.port = Integer.parseInt(props.getProperty("port"));
      configuration.readCommandsPort = Integer.parseInt(props.getProperty("readCommandsPort"));
      configuration.writeCommandsPort = Integer.parseInt(props.getProperty("writeCommandsPort"));

      configuration.enableReadCommands =
          Boolean.parseBoolean(props.getProperty("enableReadCommands"));
      configuration.enableWriteCommands =
          Boolean.parseBoolean(props.getProperty("enableWriteCommands"));
      configuration.enableWriteCommandsToRegisters =
          props.getProperty("enableCoilsForWriteCommands");

      configuration.sendAckToMODBUS40 =
          Boolean.parseBoolean(props.getProperty("sendAckToMODBUS40"));
      configuration.sendAckToRMU40 = Boolean.parseBoolean(props.getProperty("sendAckToRMU40"));
      configuration.sendAckToSMS40 = Boolean.parseBoolean(props.getProperty("sendAckToSMS40"));

    } catch (IOException | NumberFormatException e) {
      // TODO log this
    } finally {
      try {
        if (resourceStream != null) {
          resourceStream.close();
        }
      } catch (IOException e) {
        // nothing to do;
      }
    }

    return configuration;
  }

  public void initialize() {
    logger.debug("Initialized Nibe Heat Pump device stand-alone handler.");
    logger.debug("Using configuration: {}", configuration.toString());

    apiThread = new Thread(api, "HandlerApiThread");
    apiThread.start();

    try {
      parseWriteEnabledItems();
      // TODO add support for different connectors, currently fixed to UDP
      // connector = ConnectorFactory.getConnector(thing.getThingTypeUID());
      connector = new UDPConnector();
    } catch (IllegalArgumentException e) {
      // String description = String.format("Illegal configuration, %s",
      // e.getMessage());
      updateStatus(Status.OFFLINE);
      return;
    }

    itemsToPoll.clear();
    // TODO: add support for querying parameters
    // itemsToPoll.addAll(this.getThing().getChannels().stream().filter(c ->
    // isLinked(c.getUID())).map(c -> {
    // int coilAddress = parseCoilAddressFromChannelUID(c.getUID());
    // logger.debug("Linked channel '{}' found, register '{}'",
    // c.getUID().getAsString(),
    // coilAddress);
    // return coilAddress;
    // }).filter(c -> c != 0).collect(Collectors.toSet()));

    logger.debug("Linked registers {}: {}", itemsToPoll.size(), itemsToPoll);

    clearCache();

    if (connectorTask == null || connectorTask.isCancelled()) {
      connectorTask = scheduler.scheduleWithFixedDelay(() -> {
        if (reconnectRequest) {
          logger.debug("Restarting requested, restarting...");
          reconnectRequest = false;
          closeConnection();
        }

        connect();
      }, 0, 10, TimeUnit.SECONDS);
    }
  }

  private void connect() {
    if (!connector.isConnected()) {
      logger.debug("Connecting to heat pump");
      try {
        connector.addEventListener(this);
        connector.connect(configuration);
        updateStatus(Status.ONLINE);

        if (pollingJob == null || pollingJob.isCancelled()) {
          logger.debug("Start refresh task, interval={}sec", 1);
          pollingJob = scheduler.scheduleAtFixedRate(pollingRunnable, 0, 1, TimeUnit.SECONDS);
        }
      } catch (NibeHeatPumpException e) {
        logger.debug("Error occurred when connecting to heat pump, exception {}", e.getMessage());
        updateStatus(Status.OFFLINE);
      }
    } else {
      logger.debug("Connection to heat pump already open");
    }
  }

  private void closeConnection() {
    logger.debug("Closing connection to the heat pump");

    if (pollingJob != null && !pollingJob.isCancelled()) {
      pollingJob.cancel(true);
      pollingJob = null;
    }

    if (connector != null) {
      connector.removeEventListener(this);
      connector.disconnect();
    }
  }

  private void quit() {
    closeConnection();
    api.shutDown();
    apiThread.interrupt();
    System.exit(0);

  }

  @Override
  public void errorOccurred(String error) {
    logger.debug("Error '{}' occurred, re-establish the connection", error);
    reconnectRequest = true;
    updateStatus(Status.OFFLINE);
  }

  @Override
  public void msgReceived(NibeHeatPumpMessage msg) {
    try {
      if (logger.isTraceEnabled()) {
        logger.trace("Received raw data: {}", msg.toHexString());
      }

      logger.debug("Received message: {}", msg);

      updateStatus(Status.ONLINE);

      if (msg instanceof ModbusReadResponseMessage) {
        handleReadResponseMessage((ModbusReadResponseMessage) msg);
      } else if (msg instanceof ModbusWriteResponseMessage) {
        handleWriteResponseMessage((ModbusWriteResponseMessage) msg);
      } else if (msg instanceof ModbusDataReadOutMessage) {
        handleDataReadOutMessage((ModbusDataReadOutMessage) msg);
      } else {
        logger.debug("Received unknown message: {}", msg.toString());
      }
    } catch (Exception e) {
      logger.debug("Error occurred when parsing received message, reason: {}", e.getMessage());
    }
  }

  private void handleReadResponseMessage(ModbusReadResponseMessage msg) {
    if (readResult != null) {
      readResult.set(msg);
      VariableInformation variableInfo =
          VariableInformation.getVariableInfo(pumpModel, msg.getCoilAddress());
      if (variableInfo != null) {
        api.handleCommandResponse(
            "READ " + msg.getCoilAddress() + " " + msg.getValue() / variableInfo.factor);
      } else {
        api.handleCommandResponse("ERROR: UNKNOWN REGISTER");
      }
    }
  }

  private void handleWriteResponseMessage(ModbusWriteResponseMessage msg) {
    if (writeResult != null) {
      writeResult.set(msg);
      api.handleCommandResponse(String.valueOf(msg.isSuccessful()).toUpperCase());
    }
  }

  private void handleDataReadOutMessage(ModbusDataReadOutMessage msg) {
    boolean parse = true;

    logger.debug("Received data read out message");
    if (configuration.throttleTime > 0) {
      if ((lastUpdateTime + configuration.throttleTime) > System.currentTimeMillis()) {
        logger.debug("Skipping data read out message parsing");
        parse = false;
      }
    }

    if (parse) {
      logger.debug("Parsing data read out message");
      lastUpdateTime = System.currentTimeMillis();
      List<ModbusValue> regValues = msg.getValues();

      if (regValues != null) {
        for (ModbusValue val : regValues) {
          handleVariableUpdate(pumpModel, val);
        }
      }
    }
  }

  private void handleVariableUpdate(PumpModel pumpModel, ModbusValue value) {
    logger.debug("Received variable update: {}", value);
    int coilAddress = value.getCoilAddress();

    VariableInformation variableInfo = VariableInformation.getVariableInfo(pumpModel, coilAddress);

    if (variableInfo != null) {
      logger.trace("Using variable information to register {}: {}", coilAddress, variableInfo);

      double val = (double) value.getValue() / (double) variableInfo.factor;
      logger.debug("{} = {}", coilAddress + ":" + variableInfo.variable, val);

      CacheObject oldValue = stateMap.get(coilAddress);
      stateMap.put(coilAddress, new CacheObject(System.currentTimeMillis(), val));

      if (oldValue != null && val == oldValue.value) {
        logger.trace("Value did not change, ignoring update");
      } else {
        final String channelPrefix = (variableInfo.type == Type.SETTING ? "setting#" : "sensor#");
        final String channelId = channelPrefix + String.valueOf(coilAddress);
        // final String acceptedItemType =
        // thing.getChannel(channelId).getAcceptedItemType();
        logger.trace("Value changed, updating register '{}'", channelId);
        // State state = convertNibeValueToState(variableInfo.dataType, val,
        // acceptedItemType);
        // updateState(new ChannelUID(getThing().getUID(), channelId), state);
      }
    } else {
      logger.debug("Unknown register {}", coilAddress);
    }
  }

  private final Runnable pollingRunnable = new Runnable() {
    @Override
    public void run() {

      if (!configuration.enableReadCommands) {
        logger.trace("All read commands denied, skip polling!");
        return;
      }

      List<Integer> items;
      synchronized (itemsToPoll) {
        items = new ArrayList<>(itemsToPoll);
      }

      for (int item : items) {
        if (connector != null && connector.isConnected()) {

          CacheObject oldValue = stateMap.get(item);
          if (oldValue == null
              || (oldValue.lastUpdateTime + refreshIntervalMillis()) < System.currentTimeMillis()) {

            // it's time to refresh data
            logger.debug("Time to refresh variable '{}' data", item);

            performReadCommand(item);
          }
        }
      }
    }
  };

  private void parseWriteEnabledItems() throws IllegalArgumentException {
    itemsToEnableWrite.clear();
    if (configuration.enableWriteCommands && configuration.enableWriteCommandsToRegisters != null
        && configuration.enableWriteCommandsToRegisters.length() > 0) {
      String[] items = configuration.enableWriteCommandsToRegisters.replace(" ", "").split(",");
      for (String item : items) {
        try {
          int coilAddress = Integer.parseInt(item);
          VariableInformation variableInformation =
              VariableInformation.getVariableInfo(pumpModel, coilAddress);
          if (variableInformation == null) {
            String description = String.format("Unknown register %s", coilAddress);
            api.handleCommandResponse("ERROR: UNKNOWN WRITE REGISTER");
            throw new IllegalArgumentException(description);
          }
          itemsToEnableWrite.add(coilAddress);
        } catch (NumberFormatException e) {
          String description = String.format("Illegal register %s", item);
          throw new IllegalArgumentException(description);
        }
      }
    }
    logger.debug("Enabled registers for write commands: {}", itemsToEnableWrite);
  }

  public void handleCommand(ApiCommand command) {
    int coilAddress = command.address;
    int value = command.value;

    logger.debug("Received command {}", command);
    VariableInformation variableInfo = VariableInformation.getVariableInfo(pumpModel, coilAddress);
    switch (command.type) {
      case USAGE: // not routed but directly handled by HandlerApi 
        return;
      case QUIT:
        logger.trace("Quitting program.");
        api.handleCommandResponse("OK");
        quit();
      case INFO:
        handleInfoCommand(coilAddress);
        return;
      case REFRESH:
        logger.debug("Clearing cache value for coilAddress '{}' to refresh data", coilAddress);
        clearCache(coilAddress);
        api.handleCommandResponse("OK");
        return;
      case READ:
        if (variableInfo == null) {
          logger.warn("Reading unknown register {}; add it to model definiton first!", coilAddress);
        }
        handleReadCommand(coilAddress);
        return;
      case WRITE:
        handleWriteCommand(coilAddress, value);
        return;
      case SUBSCRIBE:
        if (variableInfo == null) {
          logger.warn("Polling unknown register {}; add it to model definiton first!", coilAddress);
        }
        if (!itemsToPoll.contains(coilAddress)) {
          itemsToPoll.add(coilAddress);
        }
        api.handleCommandResponse("OK");
        return;
      case UNSUBSCRIBE:
        synchronized (itemsToPoll) {
          int delPos = itemsToPoll.indexOf(coilAddress);
          if (delPos >= 0) {
            itemsToPoll.remove(delPos);
          }
        }
        api.handleCommandResponse("OK");
        return;
      case QUERY:
        handleQueryCommand(coilAddress);
        return;
      case QUERYALL:
        handleQueryAllCommand();
        return;
    }

  }

  private void handleInfoCommand(int coilAddress) {
    VariableInformation info = VariableInformation.getVariableInfo(pumpModel, coilAddress);
    if (info != null) {
      api.handleCommandResponse("INFO " + coilAddress + " \"" + info.variable + "\";"
          + info.dataType + ";" + info.minValue + ";" + info.maxValue + ";" + info.defaultValue);
    } else {
      api.handleCommandResponse("ERROR: UNKNOWN REGISTER");
    }
  }

  private void handleQueryCommand(int coilAddress) {
    CacheObject cachedRegister = stateMap.get(coilAddress);

    if (cachedRegister != null) {
      api.handleCommandResponse("QUERY " + coilAddress + " " + cachedRegister.value);
    } else {
      api.handleCommandResponse("ERROR: UNKNOWN REGISTER");
    }
  }

  private void handleQueryAllCommand() {
    StringBuffer sb = new StringBuffer("QUERY ");
    for (Integer id : stateMap.keySet()) {
      sb.append(id + " " + stateMap.get(id).value + " ");
    }

    sb.deleteCharAt(sb.length() - 1); // remove trailing space
    api.handleCommandResponse(sb.toString());
  }

  private void handleReadCommand(int coilAddress) {
    if (!configuration.enableReadCommands) {
      logger.info(
          "All read commands denied, ignoring command! Change Nibe heat pump binding configuration if you want to enable read commands.");
      api.handleCommandResponse("ERROR: DISABLED");
      return;
    }

    performReadCommand(coilAddress);
  }

  private synchronized void performReadCommand(int item) {
    ModbusReadRequestMessage request =
        new ModbusReadRequestMessage.MessageBuilder().coilAddress(item).build();

    try {
      readResult = sendMessageToNibe(request);
      ModbusReadResponseMessage result =
          (ModbusReadResponseMessage) readResult.get(TIMEOUT, TimeUnit.MILLISECONDS);
      if (result != null) {
        if (request.getCoilAddress() != result.getCoilAddress()) {
          logger.debug("Data from wrong register '{}' received, expected '{}'",
              result.getCoilAddress(), request.getCoilAddress());
        }
        // update variable anyway
        handleVariableUpdate(pumpModel, result.getValueAsModbusValue());
      }
    } catch (TimeoutException e) {
      logger.debug("Message sending to heat pump failed, no response");
      updateStatus(Status.OFFLINE);
    } catch (InterruptedException e) {
      logger.debug("Message sending to heat pump failed, sending interrupted");
    } catch (NibeHeatPumpException e) {
      logger.debug("Message sending to heat pump failed, exception {}", e.getMessage());
      updateStatus(Status.OFFLINE);
    } finally {
      readResult = null;
    }
  }

  private void handleWriteCommand(int coilAddress, int value) {
    if (!configuration.enableWriteCommands) {
      logger.info(
          "All write commands denied, ignoring command! Change Nibe heat pump binding configuration if you want to enable write commands.");
      api.handleCommandResponse("ERROR: DISABLED");
      return;
    }

    if (!itemsToEnableWrite.contains(coilAddress)) {
      logger.info(
          "Write commands to register '{}' not allowed, ignoring command! Add this register to Nibe heat pump binding configuration if you want to enable write commands.",
          coilAddress);
      api.handleCommandResponse("ERROR: DISABLED REGISTER");
      return;
    }

    if (connector != null) {
      VariableInformation variableInfo =
          VariableInformation.getVariableInfo(pumpModel, coilAddress);
      logger.debug("Usig variable information for register {}: {}", coilAddress, variableInfo);

      if (variableInfo != null && variableInfo.type == VariableInformation.Type.SETTING) {
        value = value * variableInfo.factor;

        ModbusWriteRequestMessage msg = new ModbusWriteRequestMessage.MessageBuilder()
            .coilAddress(coilAddress).value(value).build();

        try {
          writeResult = sendMessageToNibe(msg);
          ModbusWriteResponseMessage result =
              (ModbusWriteResponseMessage) writeResult.get(TIMEOUT, TimeUnit.MILLISECONDS);
          if (result != null) {
            if (result.isSuccessful()) {
              logger.debug("Write message sending to heat pump succeeded");
            } else {
              logger.error(
                  "Message sending to heat pump failed, value not accepted by the heat pump");
            }
          } else {
            logger.debug("Something weird happen, result for write command is null");
          }
        } catch (TimeoutException e) {
          logger.warn("Message sending to heat pump failed, no response");
          updateStatus(Status.OFFLINE);
        } catch (InterruptedException e) {
          logger.debug("Message sending to heat pump failed, sending interrupted");
        } catch (NibeHeatPumpException e) {
          logger.debug("Message sending to heat pump failed, exception {}", e.getMessage());
          updateStatus(Status.OFFLINE);
        } finally {
          writeResult = null;
        }

        // Clear cache value to refresh coil data from the pump.
        // We might not know if write message have succeed or not, so let's always
        // refresh it.
        logger.debug("Clearing cache value for coilAddress '{}' to refresh channel data",
            coilAddress);
        clearCache(coilAddress);
      } else {
        logger.debug(
            "Command to coilAddress '{}' rejected, because item is read only parameter or not defined in model",
            coilAddress);
      }
    } else {
      logger.debug("No connection to heat pump");
      updateStatus(Status.OFFLINE);
    }
  }

  private synchronized NibeHeatPumpCommandResult sendMessageToNibe(NibeHeatPumpMessage msg)
      throws NibeHeatPumpException {

    logger.debug("Sending message: {}", msg);
    connector.sendDatagram(msg);
    return new NibeHeatPumpCommandResult();
  }

  private void clearCache() {
    stateMap.clear();
    lastUpdateTime = 0;
  }

  private void clearCache(int coilAddress) {
    stateMap.put(coilAddress, null);
  }

  private void updateStatus(Status status) {
    // this.status = status;
    if (status.equals(Status.OFFLINE)) {
      api.handleCommandResponse("ERROR: OFFLINE");
    }
  }

  private long refreshIntervalMillis() {
    return configuration.refreshInterval * 1000;
  }

}
