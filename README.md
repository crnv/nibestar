# NibeStar - The NIBE STand Alone binding handleR

NibeStar is used to control some heat pump models from Nibe. It is based on the great work by the
openHAB project (see Acknowledgments). 

CAVEAT: this is an unofficial project that has no connection to 

## Getting Started

Check out the project source files and run gradle.

### Prerequisites

You need to have the openHAB Nibe Modbus Gateway running that communicates with the heat pump and
translates betweeen Modbus and UDP. Instructions on how to build and install such a gateway can
be found at <https://www.openhab.org/addons/bindings/nibeheatpump/#prerequisites>

### Installing

Run gradle 'fatJar' to create a jar file containing all dependencies.

Run the jar with the following parameters:

Windows:

```
gradlew.bat fatJar
```

Linux:

```
./gradlew fatJar
```

Copy the build jar file to the desired location.

###Configuration
NibeStar is configured using .properites files and command line parameters. Here is an example for
such an properties file, adapt it to your needs:


```
# Default properties for Nibe heat pump handler
# the model of the heat pump, supported values: F1X45, F1X55, F750, F470
pumpModel=F1X55
# console,tcp
api=console
#only for tcp console
api.port=12345
# IP of Nibe GW device
hostName=10.0.0.111
# as defined in the Nibe GW device
port=9999
readCommandsPort=9999
writeCommandsPort=10000
# interval for refreshing to subscribed values
refresherval=5
# enable/disable read commands
enableReadCommands=true
# enable/disable write commands
# WARNING: only enable, when you what you're doing. This may damage your heat pump!
enableWriteCommands=false
# list of registers (separated by ;) that are allowed to be written.
# WARNING: only add registers, when you know what you're doing. This may damage your heat pump!
enableWriteCommandsToRegisters=40940;
# use a guarding period between issuing commands (in ms); set 0 to disable it  
throttleTime=0
# leave these values set to 'false'
sendAckToMODBUS40=false
sendAckToRMU40=false
sendAckToSMS40=false
```

Store the configuration in a file such as "custom.properties" and provide its path when starting
the program. 

## Usage
Indicate the path to the configuration file, the heat pump model and the IP address of the Nibe
Modbus Gateway.

Windows:

```
java -jar NibeStar-all.jar -pumpModel=11X5 api=console configuration="C:\path\to\custom.properties"
```

Linux:

```
java -jar NibeStar-all.jar -pumpModel=11X5 api=console configuration="path/to/custom.properties"
```

If the API could be started, the program will welcome you with the following message:

```
NibeStar: Unofficial Nibe Modbus Interface
READY (enter USAGE to see usage information)
```

In the case of an error (e.g., the UDP communication to the Nibe Modbus Gateway failed), the 
following message will be displayed: 

```
NibeStar: Unofficial Nibe Modbus Interface
READY (enter USAGE to see usage information)
ERROR: OFFLINE
```

##API Usage

```
usage: this API supports the following commands:
	INFO id: prints information about the variable with the given id
	QUERY id: query the variable with the given id. The variable has to be included in the LOG.SET file or been manually read out before.
	QUERYALL id: query all previously read variables.
	QUIT: quit the program.
	READ id: read the variable with the given id. Caveat: the most important variables should be added to LOG.SET file, to be reported periodically by the heat pump
	SUBSCRIBE id: periodically read the variable with the given id.
	UNSUBSCRIBE id: stop reading the variable with the given id periodically.
	USAGE: print this usage information.
	WRITE id value: set the variable with the given id to the defined value. Caveat: use with caution!
```

## Running the tests

This project contains JUnit tests. 


## Contributing

Contributions are welcome!

## Authors

* **the openHAB contributors** - *Nibe heat pump binding* - [NibeHeatpumpBinding](https://github.com/openhab/openhab2-addons/tree/master/addons/binding/org.openhab.binding.nibeheatpump)
* **chrismo**

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the Eclipse Public License 2.0. See NOTICE for details.

## Acknowledgments

* The openHAB project: <https://www.openhab.org/>
* The best Austrian community for house builders, who don't exactly know what they're doing, but they're doing it better :-) <https://www.energiesparhaus.at/denkwerkstatt/forum.asp>